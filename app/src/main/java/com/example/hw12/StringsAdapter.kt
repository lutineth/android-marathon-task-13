package com.example.hw12

import android.os.Build
import android.view.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.hw12.databinding.TextrecItemBinding
import kotlin.random.Random

class StringsAdapter: RecyclerView.Adapter<StringsAdapter.TextRecHolder>() {
    private val textRecList = ArrayList<StringsRecView>()
    class TextRecHolder(item: View): RecyclerView.ViewHolder(item) {
        private val binding = TextrecItemBinding.bind(item)

        @RequiresApi(Build.VERSION_CODES.M)
        fun bind(textRec: StringsRecView) = with(binding){
            tvView.text = textRec.text
            tvView.setTextAppearance(textRec.style)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TextRecHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.textrec_item, parent, false)
        return TextRecHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: TextRecHolder, position: Int) {
        for(i in textRecList){
            when(Random.nextInt(1,20)%3){
                0 -> i.style = R.style.Style1
                1 -> i.style = R.style.Style2
                2 -> i.style = R.style.Style3
            }
        }
        holder.bind(textRecList[position])
    }

    fun addTextRec(textRec: StringsRecView){
        textRecList.add(textRec)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return textRecList.size
    }

}