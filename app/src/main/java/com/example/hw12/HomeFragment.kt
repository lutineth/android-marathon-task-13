package com.example.hw12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.*
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hw12.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private val adapter = StringsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater)
        binding.bottomNavig.selectedItemId = R.id.home
        binding.bottomNavig.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.exit -> {
                    Navigation.findNavController(binding.root).navigate(R.id.loginFragmentNH)
                }
                R.id.home -> { }
                R.id.web -> {
                    Navigation.findNavController(binding.root).navigate(R.id.webFragmentNH)
                }
            }
            true
        }
        init()
        return binding.root
    }

    private fun init(){
        binding.apply{
            rcTextView.layoutManager = LinearLayoutManager(this.root.context)
            rcTextView.adapter = adapter
            btAddView.setOnClickListener{
                adapter.addTextRec(StringsRecView((etView.text.toString()), R.style.Style1))
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }
}