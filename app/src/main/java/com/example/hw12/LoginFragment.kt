package com.example.hw12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.*
import androidx.core.view.isVisible
import androidx.navigation.Navigation
import com.example.hw12.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentLoginBinding.inflate(inflater)
        binding.bottomMenu.isVisible = false;
        binding.tvLogin.setOnClickListener{
            Navigation.findNavController(binding.root).navigate(R.id.registrationFragmentNL)
        }
        binding.bLogin.setOnClickListener{
            Navigation.findNavController(binding.root).navigate(R.id.homeFragmentNL)
        }
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance() = LoginFragment()
    }
}
